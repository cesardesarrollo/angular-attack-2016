import {Component, Injector, OnInit} from "angular2/core";
import {ControlGroup, FormBuilder, Validators} from "angular2/common";
import {DataService} from "../shared/data.service";

@Component({
    template: `
        <form class="form" [ngFormModel]="myForm" (ngSubmit)="onSaveData()">
            <div class="input-group">
                <label for="title">Gasto</label>
                <input class="form-control" [ngFormControl]="myForm.find('title')" type="text" id="title" #title="ngForm">
            </div>
            <div class="input-group">
                <label for="content">Costo</label>
                <input class="form-control" [ngFormControl]="myForm.find('content')" type="text" id="content">
            </div>
            <br>
            <button type="submit" [disabled]="!myForm.valid">Agregar gasto</button>
        </form>
    `,
    providers: [DataService]
})
export class ProtectedComponent implements OnInit {
    myForm: ControlGroup;

    constructor(private _fb: FormBuilder, private _dataService: DataService) {}

    onSaveData() {
        this._dataService.addData(this.myForm.value)
            .subscribe(
                data => console.log(data),
                error => console.error(error)
            );
    }

    ngOnInit():any {
        this.myForm = this._fb.group({
           title: ['', Validators.required],
           content: ['', Validators.required],
        });
    }
}