import {Component} from "angular2/core";
import {DataService} from "../shared/data.service";

@Component({
    template: `
        <h1>Listado de gastos pagados</h1>
        <ul>
            <li *ngFor="#item of dataset" >{{item.title}} ({{item.content}})</li>
        </ul>
        <br>
        <button (click)="onGetData()">Get Data</button>
        <br>
        <button class="btn-danger" (click)="onDeleteData()">Delete all Data</button>
    `,
    providers: [DataService]
})
// @CanActivate()
export class PagadosComponent {
    dataset: any;

    constructor (private _dataService: DataService) {}

    onGetData() {
        this._dataService.getAllData()
            .subscribe(
                data => this.dataset = data,
                error => console.error(error)
            );
    }

    onDeleteData() {
        this._dataService.deleteAllData()
            .subscribe(
                data => this.dataset = '',
                error => console.error(error)
            );
    }
}