import {Component} from "angular2/core";
import {DataService} from "../shared/data.service";

@Component({
    template: `
        <h1>Gastos</h1>
        <!--
          <ul>
            <li *ngFor="let item of items | async">
             
            </li>
          </ul>-->

        <button (click)="onGetData()">Traer datos/button>
        <br>
        <button (click)="onDeleteData()">Delete all Data</button>
    `,
    providers: [DataService]
})
export class SuperSecretComponent {
  
    dataset: any;
    constructor( private _dataService: DataService) {
    }

    onGetData() {
        this._dataService.getAllData()
            .subscribe(
                data => this.dataset = data,
                error => console.error(error)
            );
    }

    onDeleteData() {
        this._dataService.deleteAllData()
            .subscribe(
                data => this.dataset = '',
                error => console.error(error)
            );
    }
}