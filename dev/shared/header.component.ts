import {Component} from "angular2/core";
import {ROUTER_DIRECTIVES, Router} from "angular2/router";
import {AuthService} from "./auth.service";

@Component({
    selector: 'my-header',
    template: `
        <header>
            <nav class="navbar navbar-inverse">
              <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#">Control de gastos</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li *ngIf="isAuth()"><a [routerLink]="['Protected']">Añadir Gasto</a></li>
                    <li *ngIf="isAuth()"><a [routerLink]="['SuperSecret']">Listar Gastos Por Pagar</a></li>
                  </ul>
                  <ul class="nav navbar-nav navbar-right">
                    <li *ngIf="!isAuth()" class="pull-right"><a [routerLink]="['Signup']">Sign Up</a></li>
                    <li *ngIf="!isAuth()" class="pull-right"><a [routerLink]="['Signin']">Sign In</a></li>
                    <li *ngIf="isAuth()" class="pull-right"><a (click)="logout()">Logout</a></li>
                  </ul>
                </div>
              </div>
            </nav>
        </header>
    `,
    directives: [ROUTER_DIRECTIVES]
})
export class HeaderComponent {
    constructor(private _authService: AuthService) {}

    isAuth() {
        return this._authService.isAuthenticated();
    }

    logout() {
        this._authService.logout();
    }
}